module rocksdb.filterpolicy;

import rocksdb.binding;

/**
 * Right now we don't allow subclassing the FilterPolicy but we've
 * structured the API in such a way we could add it without causing
 * any breakage.
 */
public abstract class FilterPolicy
{

    /**
     * Return the native policy handle
     */
    pure @property rocksdb_filterpolicy_t* rfp() @safe nothrow
    {
        return _rfp;
    }

    ~this()
    {
        if (_rfp is null)
        {
            return;
        }
        // rocksdb_filterpolicy_destroy(_rfp);
        _rfp = null;
    }

protected:

    /**
     * Update the native policy handle
     */
    pure @property void rfp(rocksdb_filterpolicy_t* rfp) @nogc nothrow
    {
        _rfp = rfp;
    }

private:

    rocksdb_filterpolicy_t* _rfp = null;
}

/**
 * Partial bloom filter
 */
public final class BloomFilterPolicy : FilterPolicy
{
    @disable this();

    /**
     * Construct a partial bloom filter with nBits per key
     */
    this(int nBits)
    {
        rfp = rocksdb_filterpolicy_create_bloom(nBits);
    }
}

/**
 * Full bloom filter
 */
public final class FullBoomFilterPolicy : FilterPolicy
{
    @disable this();

    /**
     * Construct a full bloom filter with nBits per key
     */
    this(int nBits)
    {
        rfp = rocksdb_filterpolicy_create_bloom_full(nBits);
    }
}
