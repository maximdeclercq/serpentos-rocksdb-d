#!/usr/bin/env bash

# Autoformat the code.
dub run --verror --skip-registry=all dfmt -- $(find src -name '*.d')

# Check we have no typos.
command -v misspell 2>&1 >/dev/null
if [[ $? -eq 0 ]]; then
    misspell -error $(find src -name '*.d')
fi

# Nuke .orig files from modification
find . -name '*.d.orig' | xargs -I{} rm {}
